package it.unifi.p20132014.analysis.function;

import it.unifi.p20132014.analysis.agati_marchiani.OutOfDomainException;

public class SinOnX extends DifferentiableFunction {
	public double getFPrime(double x) {
		return (x * Math.cos(x) - Math.sin(x)) / Math.pow(x, 2);
	}

	public double getF(double x) { return x == 0 ? 1 : Math.sin(x) / x; }
}
