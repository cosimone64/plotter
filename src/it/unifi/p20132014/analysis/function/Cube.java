package it.unifi.p20132014.analysis.function;

import it.unifi.p20132014.analysis.agati_marchiani.OutOfDomainException;

public class Cube extends DifferentiableFunction {
	@Override
	public double getFPrime(double x) { return 3 * (Math.pow(x, 2)); }

	@Override
	public double getF(double x) { return Math.pow(x, 3); }
}
