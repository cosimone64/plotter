package it.unifi.p20132014.analysis.function;

public abstract class DifferentiableFunction extends Function {
	/**
	 * This method returns the value of the derivative of the function in the
	 * desired point. The value will be different for every function.
	 * 
	 * @param x
	 *            The number to calculate the function
	 * @return The value of the function
	 */
	public abstract double getFPrime(double x);
}
