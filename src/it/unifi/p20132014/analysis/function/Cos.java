package it.unifi.p20132014.analysis.function;

import it.unifi.p20132014.analysis.agati_marchiani.OutOfDomainException;

public class Cos extends DifferentiableFunction {
	@Override
	public double getFPrime(double x) { return -(Math.sin(x)); }

	@Override
	public double getF(double x) { return Math.cos(x); }
}
