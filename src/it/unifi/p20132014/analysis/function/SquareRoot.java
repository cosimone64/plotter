package it.unifi.p20132014.analysis.function;

import it.unifi.p20132014.analysis.agati_marchiani.OutOfDomainException;

public class SquareRoot extends DifferentiableFunction {
	public double getFPrime(double x) {
		return (1 / 2) * (1 / Math.sqrt(x));
	}

	public double getF(double x) throws OutOfDomainException {
		if (x < 0) throw new OutOfDomainException();
		return Math.sqrt(x);
	}
}
