package it.unifi.p20132014.analysis.function;

import it.unifi.p20132014.analysis.agati_marchiani.OutOfDomainException;

public class Abs extends Function {
	@Override
	public double getF(double x) { return x < 0 ? -x : x; }
}
