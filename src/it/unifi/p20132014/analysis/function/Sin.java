package it.unifi.p20132014.analysis.function;

public class Sin extends DifferentiableFunction
{
	public double getFPrime(double x) { return Math.cos(x); }

	public double getF(double x) { return Math.sin(x); }
}
