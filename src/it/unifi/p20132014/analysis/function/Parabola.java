package it.unifi.p20132014.analysis.function;

import it.unifi.p20132014.analysis.agati_marchiani.OutOfDomainException;

public class Parabola extends DifferentiableFunction {
	public double getFPrime(double x) { return 2 * x; }

	public double getF(double x) { return Math.pow(x, 2); }
}
