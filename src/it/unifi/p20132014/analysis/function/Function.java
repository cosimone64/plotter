package it.unifi.p20132014.analysis.function;

import it.unifi.p20132014.analysis.agati_marchiani.OutOfDomainException;

public abstract class Function {
	/**
	 * This method returns the value of the function in the desired point. The
	 * value will be different for every function.
	 * 
	 * @param x
	 *            The number to calculate the function
	 * @return The value of the function in 'x'
	 * @throws OutOfDomainException
	 *             This exception is thrown if 'x' is out of the domain of the
	 *             current function
	 */
	public abstract double getF(double x) throws OutOfDomainException;
}
