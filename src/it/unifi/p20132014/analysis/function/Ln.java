package it.unifi.p20132014.analysis.function;

import it.unifi.p20132014.analysis.agati_marchiani.OutOfDomainException;

public class Ln extends DifferentiableFunction {
	public double getFPrime(double x) { return (1 / Math.abs(x)); }

	public double getF(double x) throws OutOfDomainException {
		if (Double.isNaN(Math.log(x)) && (x != 0))
			throw new OutOfDomainException();
		// this value is set to allow '0' to be used as left extreme
		return x == 0 ? Math.log(0.001) : Math.log(x);
	}
}
