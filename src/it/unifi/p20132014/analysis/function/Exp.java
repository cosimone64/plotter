package it.unifi.p20132014.analysis.function;

public class Exp extends DifferentiableFunction {
	@Override
	public double getFPrime(double x) { return Math.exp(x); }

	@Override
	public double getF(double x) { return Math.exp(x); }
}
