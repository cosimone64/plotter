package it.unifi.p20132014.analysis.agati_marchiani;

import it.unifi.p20132014.analysis.function.DifferentiableFunction;
import it.unifi.p20132014.analysis.function.Function;

import javax.swing.JOptionPane;

public class Calculator
{
	private CartesianPlane cp;
	private Drawer d;
	private double result;
	private Class<?> c;
	public double startPoint;
	public int iterations;
	public double epsilon;

	public Calculator(CartesianPlane cp, Drawer d) {
		this.cp = cp;
		this.d = d;
	}

	public void getFunction(String function, boolean diff) {
		try {
			c = Class.forName(function);
			if (diff) {
				try {
					d.f = (DifferentiableFunction) c.newInstance();
				}
				catch (ClassCastException e) {
					JOptionPane.showMessageDialog(cp,
					        "Function is not differentiable", "ERROR",
					        JOptionPane.ERROR_MESSAGE);
				}
			}
			else {
				d.f = (Function) c.newInstance();
			}
		}
		catch (ClassNotFoundException e) {
			JOptionPane.showMessageDialog(cp, "Invalid Class Name", "ERROR",
			        JOptionPane.ERROR_MESSAGE);
		}
		catch (InstantiationException e) {
			JOptionPane.showMessageDialog(cp, "Invalid Class Name", "ERROR",
			        JOptionPane.ERROR_MESSAGE);
		}
		catch (IllegalAccessException e) {
			JOptionPane.showMessageDialog(cp, "Invalid Class Name", "ERROR",
			        JOptionPane.ERROR_MESSAGE);
		}
		catch (NoClassDefFoundError e) {
			JOptionPane.showMessageDialog(cp, "Invalid Class Name", "ERROR",
			        JOptionPane.ERROR_MESSAGE);
		}
	}

	public void calculateLeft(int frames, double frameLength,
	        double[] frameValue) {
		result = 0;
		for (int index = 0; index < frames; index++)
			result = result + (frameLength * (frameValue[index]));
		JOptionPane.showMessageDialog(cp, "Left integral value: " + result,
		        "Result", JOptionPane.INFORMATION_MESSAGE);
	}

	public void calculateRight(int frames, double frameLength,
	        double[] frameValue) {
		result = 0;
		for (int index = 1; index < frames - 1; index++)
			result = result + (frameLength * (frameValue[index + 1]));
		JOptionPane.showMessageDialog(cp, "Right integral value: " + result,
		        "Result", JOptionPane.INFORMATION_MESSAGE);
	}

	public void calculateMiddle(int frames, double frameLength,
	        double[] frameValue) {
		result = 0;
		for (int index = 0; index < frames - 1; index++)
			result = result + (frameLength
			        * ((frameValue[index]) + (frameValue[index + 1])) / 2);
		JOptionPane.showMessageDialog(cp, "Middle integral value: " + result,
		        "Result", JOptionPane.INFORMATION_MESSAGE);
	}

	public void calculateTrap(int frames, double frameLength,
	        double[] frameValue) {
		result = 0;
		for (int index = 1; index < frames; index++)
			result = result + ((frameValue[index - 1] + frameValue[index])
			        * (frameLength) / 2);
		JOptionPane.showMessageDialog(cp,
		        "Trapezoidal integral value: " + result, "Result",
		        JOptionPane.INFORMATION_MESSAGE);
	}

	public void calculateNewton(DifferentiableFunction f, double x0, int iter,
	        double epsilon) {
		int i = 0;
		double x1 = 0;
		d.newtonXValues = new double[iter];
		d.newtonYValues = new double[iter];
		try {
			if (Math.abs(f.getF(x0)) < epsilon) {
				JOptionPane.showMessageDialog(cp, "Root in " + x0, "Result",
				        JOptionPane.INFORMATION_MESSAGE);
				return;
			}
			while (i < iter) {
				d.newtonXValues[i] = x0;
				d.newtonYValues[i] = f.getF(x0);
				i++;
				if (f.getFPrime(x0) == 0) {
					JOptionPane.showMessageDialog(cp,
					        "Reached null derivative.", "ERROR",
					        JOptionPane.ERROR_MESSAGE);
					return;
				}
				x1 = (x0 - (f.getF(x0) / f.getFPrime(x0)));
				if ((Math.abs(x1 - x0) > epsilon) && (f.getF(x1) > epsilon)) {
					x0 = x1;
				} else {
					JOptionPane.showMessageDialog(cp, "Root in " + x1, "Result",
					        JOptionPane.INFORMATION_MESSAGE);
					return;
				}
			}
			JOptionPane.showMessageDialog(cp, "Too many iterations.", "ERROR",
			        JOptionPane.ERROR_MESSAGE);
		}
		catch (OutOfDomainException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
