package it.unifi.p20132014.analysis.agati_marchiani;

import java.awt.BorderLayout;
import javax.swing.*;

@SuppressWarnings("serial")
public class Plotter extends JFrame {
	private CartesianPlane cp;
	private LowerPanel lp;
	private UpperPanel up;
	private Drawer d;
	private Calculator c;

	/**
	 * This is the constructor of the main frame of the program. A cartesian
	 * plane, a lower panel and an upper panel are added to the frame. The frame
	 * is set not to be resizable, to keep the functions scaled correctly.
	 * 
	 * @param name
	 *            The name of the window
	 */
	public Plotter(String name) {
		super(name);
		d = new Drawer();
		cp = new CartesianPlane(d);
		c = new Calculator(cp, d);
		up = new UpperPanel(cp);
		lp = new LowerPanel(cp, d, up, c);
		setSize(1024, 576);
		setLocationRelativeTo(null);
		setResizable(false);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		add(cp, BorderLayout.CENTER);
		add(lp, BorderLayout.SOUTH);
		add(up, BorderLayout.NORTH);
	}

	public CartesianPlane getPlane() { return cp; }

	public LowerPanel getLow() { return lp; }

	public UpperPanel getUp() { return up; }

	public Drawer getDraw() { return d; }

	public Calculator getCalc() { return c; }

	/**
	 * This is our main method. A Plotter object is created, and set visible for
	 * the user.
	 * 
	 * @param args
	 *            The string array passed to the main method
	 */
	public static void main(String[] args) {
		Plotter p = new Plotter("Plotter");
		p.setVisible(true);
		JOptionPane.showMessageDialog(p, "Welcome!");
	}
}
