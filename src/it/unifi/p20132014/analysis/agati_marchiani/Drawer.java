package it.unifi.p20132014.analysis.agati_marchiani;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JOptionPane;
import it.unifi.p20132014.analysis.function.DifferentiableFunction;
import it.unifi.p20132014.analysis.function.Function;

public class Drawer {
	private double xGrid;
	private double yGrid;
	private Class<?> c;
	private double locFLeft;
	private double locFRight;
	private int[] trapX;
	private int[] trapY;
	public int operation;
	public double result;
	public double[] frameValue;
	public double[] newtonYValues;
	public double[] newtonXValues;
	public int frames;
	public double left;
	public double right;

	public double frameLength;
	public final int OFFSETX;
	public final int OFFSETY;
	public double scaleX;
	public double scaleY;
	public Function f;
	public int index;

	public Drawer() {
		OFFSETX = 510;
		OFFSETY = 230;
		xGrid = 10;
		yGrid = 10;

		scaleX = 10;
		scaleY = 10;
		trapX = new int[4];
		trapY = new int[4];
	}

	/**
	 * This method creates an instance from the "Function" or
	 * "DifferentiableFunction" class, depending on the state of the checkbox,
	 * reading its name from a string.
	 * 
	 * @param function
	 *            The name of the function to be created
	 */
	public void getFunction(String function, boolean diff) {
		try {
			c = Class.forName(function);
			if (diff) {
				try {
					f = (DifferentiableFunction) c.newInstance();
				} catch (ClassCastException e) {
					JOptionPane.showMessageDialog(null,
					        "Function is not differentiable", "ERROR",
					        JOptionPane.ERROR_MESSAGE);
				}
			}
			else {
				f = (Function) c.newInstance();
			}
		}
		catch (ClassNotFoundException e) {
			JOptionPane.showMessageDialog(null, "Invalid Class Name", "ERROR",
			        JOptionPane.ERROR_MESSAGE);
		}
		catch (InstantiationException e) {
			JOptionPane.showMessageDialog(null, "Invalid Class Name", "ERROR",
			        JOptionPane.ERROR_MESSAGE);
		}
		catch (IllegalAccessException e) {
			JOptionPane.showMessageDialog(null, "Invalid Class Name", "ERROR",
			        JOptionPane.ERROR_MESSAGE);
		}
		catch (NoClassDefFoundError e) {
			JOptionPane.showMessageDialog(null, "Invalid Class Name", "ERROR",
			        JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * This method scales the values of the function, so that it stays true to
	 * the original proportions. The value of the scale is based on the minimum
	 * absolute value between the extremes and the maximum and minimum value of
	 * the function.
	 * 
	 * @throws OutOfDomainException
	 *             This exception is thrown if the 'x' value of the function is
	 *             out of domain.
	 */
	public void scaleExtremes() throws OutOfDomainException {
		double yMax = 0;
		double yMin = 0;
		for (double i = left; i <= right; i = i + frameLength) {
			if (f.getF(i) > yMax) yMax = f.getF(i);
			if (f.getF(i) < yMin) yMin = f.getF(i);
		}
		if (Math.abs(yMax) >= Math.abs(left)
				&& Math.abs(yMax) >= Math.abs(right)
				&& Math.abs(yMax) >= Math.abs(yMin)) {
			scaleY = (Math.abs(220 / yMax));
			scaleX = (Math.abs(220 / yMax));
		}
		if (Math.abs(yMin) >= Math.abs(left)
				&& Math.abs(yMin) >= Math.abs(right)
				&& Math.abs(yMin) >= Math.abs(yMax)) {
			scaleY = (Math.abs(220 / yMin));
			scaleX = (Math.abs(220 / yMin));
		}
		if (Math.abs(right) >= Math.abs(left)
				&& Math.abs(right) >= Math.abs(yMin)
				&& Math.abs(right) >= Math.abs(yMax)) {
			scaleX = (Math.abs(450 / right));
			scaleY = (Math.abs(450 / right));
		}
		if (Math.abs(left) >= Math.abs(yMin)
				&& Math.abs(left) >= Math.abs(right)
				&& Math.abs(left) >= Math.abs(yMax)) {
			scaleX = (Math.abs(450 / left));
			scaleY = (Math.abs(450 / left));
		}
	}

	/**
	 * This method scales the function, to fill the maximum space possible,
	 * since the user does not want to keep proportions.
	 * 
	 * @throws OutOfDomainException
	 *             This exception is thrown if the 'x' value of the function is
	 *             out of its domain
	 */
	public void unscaleExtremes() throws OutOfDomainException {
		double yMax = 0;
		double yMin = 0;
		for (double i = left; i <= right; i = i + frameLength) {
			if (f.getF(i) > yMax) { yMax = f.getF(i); }
			if (f.getF(i) < yMin) { yMin = f.getF(i); }
		}
		if (Math.abs(yMax) >= Math.abs(yMin))
			scaleY = (Math.abs(220 / yMax));
		else
			scaleY = (Math.abs(220 / yMin));
		if (Math.abs(left) >= Math.abs(right))
			scaleX = (Math.abs(450 / left));
		else
			scaleX = (Math.abs(450 / right));
	}

	/**
	 * This method scales the space between grid lines according to the scale
	 * used.
	 */
	public void scaleGrid() {
		xGrid = scaleX;
		yGrid = scaleY;
	}

	/**
	 * This method draws horizontal and vertical parallel lines to form a grid.
	 * The space between the lines is scaled according to the function.
	 * 
	 * @param g
	 *            The Graphics object to perform graphic operations
	 */
	public void drawGrid(Graphics g) {
		g.setColor(Color.LIGHT_GRAY);
		for (double i = 510; i < 960; i = i + xGrid)
			g.drawLine((int) i, 10, (int) i, 450);
		for (double i = 510; i >= 60; i = i - xGrid)
			g.drawLine((int) i, 10, (int) i, 450);
		for (double i = 230; i < 450; i = i + yGrid)
			g.drawLine(60, (int) i, 960, (int) i);
		for (double i = 230; i >= 10; i = i - yGrid)
			g.drawLine(60, (int) i, 960, (int) i);
	}

	/**
	 * This method draws the axes of the cartesian plane.
	 * 
	 * @param g
	 *            the Graphics object to draw
	 */
	public void drawAxes(Graphics g) {
		g.setColor(Color.BLACK);
		g.setColor(Color.BLACK);
		g.drawLine(510, 10, 510, 450);
		g.drawLine(60, 230, 960, 230);
		g.drawString("0", 500, 245);
		g.drawString("x", 940, 250);
		g.drawString("y", 490, 30);
	}

	/**
	 * This method draws the desired function on the cartesian plane. This is
	 * done by connecting with a line as many points as specified by the user.
	 * 
	 * @param g
	 *            The Graphics object to draw
	 * @throws OutOfDomainException
	 *             This exception is thrown if the 'x' value of the function is
	 *             out of domain
	 * @throws WrongExtremesException
	 *             This exception is thrown if the left extreme is larger than
	 *             the right one
	 */
	public void drawFunction(Graphics g)
	        throws OutOfDomainException, WrongExtremesException {
		if (left > right)
			throw new WrongExtremesException();
		g.setColor(Color.RED);
		for (double i = left; i <= right - frameLength; i = i + frameLength) {
			g.drawLine((int) ((i * scaleX + OFFSETX)),
					(int) (-(f.getF(i) * scaleY) + OFFSETY),
					(int) (scaleX * (i + frameLength) + OFFSETX),
					(int) (-(f.getF(i + frameLength) * scaleY) + OFFSETY));
			frameValue[index] = f.getF(i);
			index++;
		}
	}

	/**
	 * This method repaints the external part of the cartesian plane, to cover
	 * any line that may be drawn out of its boundaries.
	 * 
	 * @param g
	 *            The Graphics object to perform graphic operations
	 */
	public void clean(Graphics g) {
		g.setColor(new Color(237, 237, 237));
		g.fillRect(0, 0, 1024, 10);
		g.fillRect(0, 451, 1024, 460);
		g.fillRect(0, 10, 60, 556);
		g.fillRect(960, 10, 60, 556);
		g.setColor(Color.BLACK);
		g.drawRect(60, 10, 900, 440);
	}

	public void leftIntegral(Graphics g) {
		index = 0;
		for (double i = left; i <= right - frameLength; i = i + frameLength) {
			g.setColor(Color.MAGENTA);
			if (frameValue[index] <= 0) {
				g.fillRect((int) ((i * scaleX + OFFSETX)), 230,
						(int) (frameLength * scaleX),
						(int) (Math.abs((frameValue[index]) * scaleY)));
				g.setColor(Color.BLACK);
				g.drawRect((int) ((i * scaleX + OFFSETX)), 230,
						(int) (frameLength * scaleX),
						(int) (Math.abs((frameValue[index]) * scaleY)));
				index++;
			} else {
				g.fillRect((int) ((i * scaleX + OFFSETX)),
						(int) (231 - frameValue[index] * scaleY),
						(int) (frameLength * scaleX),
						(int) ((frameValue[index]) * scaleY));
				g.setColor(Color.BLACK);
				g.drawRect((int) ((i * scaleX + OFFSETX)),
						(int) (231 - frameValue[index] * scaleY),
						(int) (frameLength * scaleX),
						(int) ((frameValue[index]) * scaleY));
				index++;
			}
		}
	}

	public void rightIntegral(Graphics g) {
		index = 0;
		for (double i = left; i <= right - frameLength; i = i + frameLength) {
			g.setColor(Color.MAGENTA);
			if (index < (frames - 1)) {
				if (frameValue[index] <= 0) {
					g.fillRect((int) ((i * scaleX + OFFSETX)), 230,
							(int) (frameLength * scaleX),
							(int) (Math.abs((frameValue[index + 1]) * scaleY)));
					g.setColor(Color.BLACK);
					g.drawRect((int) ((i * scaleX + OFFSETX)), 230,
							(int) (frameLength * scaleX),
							(int) (Math.abs((frameValue[index + 1]) * scaleY)));
					index++;
				} else {
					g.fillRect((int) ((i * scaleX + OFFSETX)),
							(int) (231 - frameValue[index + 1] * scaleY),
							(int) (frameLength * scaleX),
							(int) ((frameValue[index + 1]) * scaleY));
					g.setColor(Color.BLACK);
					g.drawRect((int) ((i * scaleX + OFFSETX)),
							(int) (231 - frameValue[index + 1] * scaleY),
							(int) (frameLength * scaleX),
							(int) ((frameValue[index + 1]) * scaleY));
					index++;
				}
			}
		}
	}

	public void middleIntegral(Graphics g)
	{
		index = 0;
		for (double i = left; i <= right - frameLength; i = i + frameLength) {
			g.setColor(Color.MAGENTA);
			if (index < (frames - 1)) {
				if (frameValue[index] <= 0) {
					g.fillRect((int) ((i * scaleX + OFFSETX)), 230,
							(int) (frameLength * scaleX),
							(int) (Math.abs(
									((frameValue[index] + frameValue[index + 1])
									 / 2) * scaleY)));
					g.setColor(Color.BLACK);
					g.drawRect((int) ((i * scaleX + OFFSETX)), 230,
							(int) (frameLength * scaleX),
							(int) (Math.abs(
									((frameValue[index] + frameValue[index + 1])
					                        / 2) * scaleY)));
					index++;
				} else {
					g.fillRect((int) ((i * scaleX + OFFSETX)),
							(int) (231 - (((frameValue[index]
											+ frameValue[index + 1]) / 2) * scaleY)),
							(int) (frameLength * scaleX),
							(int) (((frameValue[index] + frameValue[index + 1])
									/ 2) * scaleY));
					g.setColor(Color.BLACK);
					g.drawRect((int) ((i * scaleX + OFFSETX)),
							(int) (231 - (((frameValue[index]
											+ frameValue[index + 1]) / 2) * scaleY)),
							(int) (frameLength * scaleX),
							(int) (((frameValue[index] + frameValue[index + 1])
									/ 2) * scaleY));
					index++;
				}
			}
		}
	}

	public void trapezoidalIntegral(Graphics g) {
		index = 0;
		g.setColor(Color.BLACK);
		for (double i = left; i < right - frameLength; i = i + frameLength) {
			if (index < (frames - 1)) {
				trapX[0] = (int) (i * scaleX + OFFSETX);
				trapX[1] = (int) (i * scaleX + OFFSETX);
				trapX[2] = (int) ((i + frameLength) * scaleX + OFFSETX);
				trapX[3] = (int) ((i + frameLength) * scaleX + OFFSETX);
				trapY[0] = 0 + OFFSETY;
				trapY[1] = (int) (-(frameValue[index] * scaleY) + OFFSETY);
				index++;
				trapY[2] = (int) (-(frameValue[index] * scaleY) + OFFSETY);
				trapY[3] = 0 + OFFSETY;
				g.setColor(Color.MAGENTA);
				g.fillPolygon(trapX, trapY, 4);
				g.setColor(Color.BLACK);
				g.drawPolygon(trapX, trapY, 4);
			}
		}
	}

	public void drawNewton(Graphics g) {
		int l = newtonYValues.length;
		g.setColor(Color.ORANGE);
		for (int i = 0; i < (l - 1); i++)
			g.drawLine((int) (newtonXValues[i] * scaleX + OFFSETX),
					(int) (newtonYValues[i] * scaleY + OFFSETY),
					(int) (newtonXValues[i + 1] * scaleX + OFFSETX),
					(int) (newtonYValues[i + 1] * scaleY + OFFSETY));

	}

	public void drawPrimitive(Graphics g) throws OutOfDomainException {
		g.setColor(Color.BLUE);
		index = 1;
		locFLeft = 0;
		locFRight = 0;
		double i = left;
		while (index < frameValue.length) {
			locFRight = locFLeft + ((frameValue[index - 1] + frameValue[index])
					* (frameLength) / 2);
			g.drawLine((int) (i * scaleX + OFFSETX),
					(int) (-(locFLeft * scaleY) + OFFSETY),
					(int) (scaleX * (i + frameLength) + OFFSETX),
					(int) (-(locFRight * scaleY) + OFFSETY));
			locFLeft = locFRight;
			index++;
			i = i + frameLength;
		}
		locFRight = locFLeft
			+ ((frameValue[index - 1] + f.getF(right)) * (frameLength) / 2);
		g.drawLine((int) (i * scaleX + OFFSETX),
				(int) (-(locFLeft * scaleY) + OFFSETY),
				(int) (scaleX * (right) + OFFSETX),
				(int) (-(locFRight * scaleY) + OFFSETY));
	}

	public void newtonMethod(Graphics g, DifferentiableFunction f, double left,
	        double right, double x0, int iter, double epsilon)
	                throws OutOfDomainException {
		int i = 0;
		double x1;
		if (Math.abs(f.getF(x0)) <= epsilon) {
			if ((x0 < left) || (x0 > right))
				operation = 3;
			else
				operation = 0;
			result = x0;
			return;
		}
		g.setColor(Color.ORANGE);
		while (i < iter) {
			if (Math.abs(f.getF(x0)) <= epsilon) {
				if ((x0 < left) || (x0 > right))
					operation = 3;
				else
					operation = 0;
				result = x0;
				return;
			}
			if (f.getFPrime(x0) == 0) {
				operation = 1;
				return;
			}
			x1 = x0 - (f.getF(x0) / f.getFPrime(x0));
			g.drawLine((int) (x0 * scaleX) + OFFSETX,
					(int) (-(f.getF(x0) * scaleY) + OFFSETY),
					(int) ((x1 * scaleX) + OFFSETX), OFFSETY);
			i++;
			if ((i <= iter) && (Math.abs(x1 - x0) > epsilon)
					&& (Math.abs(f.getF(x1)) > epsilon)) {
				x0 = x1;
			} else if (i > iter) {
				operation = 2;
				return;
			} else {
				if ((x0 < left) || (x0 > right))
					operation = 3;
				else
					operation = 0;
				result = x1;
				return;
			}
		}
		operation = 2;
	}
}
