package it.unifi.p20132014.analysis.agati_marchiani;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Graphics;

@SuppressWarnings("serial")
public class CartesianPlane extends JPanel {
	private Drawer d;
	protected boolean grid;
	protected boolean proportions;
	protected boolean leftInt;
	protected boolean rightInt;
	protected boolean middleInt;
	protected boolean trapInt;
	protected boolean newton;
	protected boolean primitive;
	protected String functionName;
	public boolean diff;
	public boolean plot;

	public CartesianPlane(Drawer d) {
		this.d = d;
		diff = false;
		plot = false;
		grid = false;
		proportions = false;
		leftInt = false;
		rightInt = false;
		middleInt = false;
		trapInt = false;
		newton = false;
		primitive = false;
	}

	/**
	 * This method draws every needed item on the cartesian plane.
	 */
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(new Color(155, 255, 255));
		g.fillRect(60, 10, 900, 440);
		if (plot) {
			d.frameValue = new double[d.frames];
			d.index = 0;
			d.frameLength = (Math.abs(d.right) + Math.abs(d.left)) / d.frames;
			try {
				if (proportions) {
					d.scaleExtremes();
				} else {
					d.unscaleExtremes();
				}
				if (grid) {
					d.scaleGrid();
					d.drawGrid(g);
				}
				if (newton) {
					d.drawNewton(g);
					newton = false;
				}
				d.drawFunction(g);
				if (primitive) {
					d.drawPrimitive(g);
				}
			}
			catch (OutOfDomainException e) {
				JOptionPane.showMessageDialog(this, "Interval out of domain.",
				        "ERROR", JOptionPane.ERROR_MESSAGE);
			}
			catch (WrongExtremesException e) {
				JOptionPane.showMessageDialog(this, "Invalid Extremes values",
				        "ERROR", JOptionPane.ERROR_MESSAGE);
			}
			catch (NullPointerException e) {}
		}
		else {
			if (grid) {
				d.scaleGrid();
				d.drawGrid(g);
			}
		}
		if (leftInt) d.leftIntegral(g);
		if (rightInt) d.rightIntegral(g);
		if (middleInt) d.middleIntegral(g);
		if (trapInt) d.trapezoidalIntegral(g);
		d.drawAxes(g);
		d.clean(g);
	}
}
