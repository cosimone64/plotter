package it.unifi.p20132014.analysis.agati_marchiani;

import it.unifi.p20132014.analysis.function.DifferentiableFunction;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

@SuppressWarnings("serial")
public class LowerPanel extends JPanel implements ActionListener {
	private CartesianPlane cp;
	private Drawer d;
	private UpperPanel up;
	private Calculator c;
	private JButton draw;
	private JButton root;
	private JButton primitive;
	private JMenuBar menuBar;
	private JMenu integral_Type;
	private JMenuItem left_Integral;
	private JMenuItem right_Integral;
	private JMenuItem middle_Integral;
	private JMenuItem trapezoidal_Integral;
	private JMenuItem clear_Integral;
	private JLabel grid;
	private JLabel setPro;
	private JCheckBox grid_Check;
	private JCheckBox set_Pro_Check;

	/**
	 * This is the constructor of the component. Every element is initialized
	 * and added to the panel. Action listeners are also added.
	 */
	public LowerPanel(CartesianPlane cp, Drawer d, UpperPanel up, Calculator c) {
		this.cp = cp;
		this.d = d;
		this.up = up;
		this.c = c;
		draw = new JButton("Draw");
		root = new JButton("Root");
		primitive = new JButton("Primitive");
		menuBar = new JMenuBar();
		integral_Type = new JMenu("Integral Type");
		left_Integral = new JMenuItem("Left Integral");
		right_Integral = new JMenuItem("Right Integral");
		middle_Integral = new JMenuItem("Middle Integral");
		trapezoidal_Integral = new JMenuItem("Trapezoidal Integral");
		clear_Integral = new JMenuItem("Clear Integral");
		grid = new JLabel("Grid");
		setPro = new JLabel("Set Proportions");
		grid_Check = new JCheckBox();
		set_Pro_Check = new JCheckBox();

		add(grid);
		add(grid_Check);
		add(draw);
		add(root);
		add(primitive);

		grid_Check.addActionListener(this);
		set_Pro_Check.addActionListener(this);
		draw.addActionListener(this);
		root.addActionListener(this);
		primitive.addActionListener(this);
		left_Integral.addActionListener(this);
		right_Integral.addActionListener(this);
		middle_Integral.addActionListener(this);
		trapezoidal_Integral.addActionListener(this);
		clear_Integral.addActionListener(this);

		integral_Type.add(left_Integral);
		integral_Type.add(right_Integral);
		integral_Type.add(middle_Integral);
		integral_Type.add(trapezoidal_Integral);

		grid_Check.setActionCommand("grid");
		set_Pro_Check.setActionCommand("pro");
		draw.setActionCommand("Draw");
		root.setActionCommand("Root");
		primitive.setActionCommand("Primitive");
		left_Integral.setActionCommand("lint");
		right_Integral.setActionCommand("rint");
		middle_Integral.setActionCommand("mint");
		trapezoidal_Integral.setActionCommand("tint");
		clear_Integral.setActionCommand("cint");
		menuBar.add(integral_Type);

		add(menuBar);
		add(setPro);
		add(set_Pro_Check);
	}

	/**
	 * This method executes operations based on the user's actions. Based on the
	 * action command, a specific block is executed. If the user checks/unchecks
	 * "grid", the grid on the cartesian plane will appear/disappear. If the
	 * user presses "Draw", a new function will be drawn, based on the values
	 * written in the JTextFields of the upper panel. If the user
	 * checks/unchecks "pro", the function will be scaled to keep proportions,
	 * or stretched to fill the plane. If the user presses "Root", all
	 * iterations of the Newton method, based on the values of the JTextFields,
	 * will be drawn. A window containing the root will also appear. If the user
	 * presses "Primitive", the primitive of the current function will be drawn.
	 * If the user chooses an option from the integral menu, the chosen integral
	 * will be drawn, and a window containing the value will appear. The menu
	 * entries will also be updated.
	 */
	public void actionPerformed(ActionEvent e) {
		String action = e.getActionCommand();
		if (action.equals("grid")) {
			cp.grid = !cp.grid;
			cp.repaint();
		}
		if (action.equals("Draw")) {
			try {
				cp.functionName = "it.unifi.p20132014.analysis.function."
				        + up.text_function.getText();
				if ((Integer.parseInt(up.text_num_Draw_Points.getText()) >= 0)) {
					d.frames = Integer
					        .parseInt(up.text_num_Draw_Points.getText());
				} else {
					JOptionPane.showMessageDialog(cp, "Negative frame number.",
					        "ERROR", JOptionPane.ERROR_MESSAGE);
				}
				d.left = Double.parseDouble(up.text_left_Extreme.getText());
				d.right = Double.parseDouble(up.text_right_Extreme.getText());
				c.getFunction(cp.functionName, cp.diff);
				cp.plot = true;
				cp.newton = false;
				cp.repaint();
			}
			catch (NumberFormatException ex) {
				JOptionPane.showMessageDialog(cp,
				        "Invalid values in text field(s).", "ERROR",
				        JOptionPane.ERROR_MESSAGE);
			}
		}
		if (action.equals("pro")) {
			cp.proportions = !cp.proportions;
			cp.repaint();
		}
		if (action.equals("Root")) {
			if (!cp.plot) {
				JOptionPane.showMessageDialog(cp, "No function drawn.", "ERROR",
				        JOptionPane.ERROR_MESSAGE);
				return;
			}
			if (!cp.diff) {
				JOptionPane.showMessageDialog(cp,
				        "Function not declared as differentiable.", "ERROR",
				        JOptionPane.ERROR_MESSAGE);
				return;
			}
			if (!(d.f instanceof DifferentiableFunction)) {
				JOptionPane.showMessageDialog(cp,
				        "Function is not differentiable.", "ERROR",
				        JOptionPane.ERROR_MESSAGE);
				return;
			}
			cp.newton = true;
			if (cp.newton) {
				try {
					c.iterations = Integer
					        .parseInt(up.text_num_Iter_Rect.getText());
					c.startPoint = Double
					        .parseDouble(up.text_starting_Point.getText());
					c.epsilon = Double.parseDouble(up.text_epsilon.getText());
					c.calculateNewton((DifferentiableFunction) d.f,
					        c.startPoint, c.iterations, c.epsilon);
				}
				catch (NumberFormatException ex) {
					JOptionPane.showMessageDialog(cp,
					        "Invalid values in text field(s).", "ERROR",
					        JOptionPane.ERROR_MESSAGE);
				}
			}
			/*
			 * if (!cp.newton) { } else { switch (c.operation) { case (0):
			 * JOptionPane.showMessageDialog(cp, "Root in " + d.result,
			 * "Result", JOptionPane.INFORMATION_MESSAGE); break; case (1):
			 * JOptionPane.showMessageDialog(cp, "Reached null derivative.",
			 * "ERROR", JOptionPane.ERROR_MESSAGE); break; case (2):
			 * JOptionPane.showMessageDialog(cp, "Too many iterations.",
			 * "ERROR", JOptionPane.ERROR_MESSAGE); case (3):
			 * JOptionPane.showMessageDialog(cp, "Root in " + d.result + " " +
			 * "(Out of extremes)", "Result", JOptionPane.INFORMATION_MESSAGE);
			 * } }
			 */
		}
		if (action.equals("Primitive")) {
			if (!cp.plot) {
				JOptionPane.showMessageDialog(cp, "No function drawn.", "ERROR",
				        JOptionPane.ERROR_MESSAGE);
				return;
			}
			cp.primitive = !(cp.primitive);
			cp.repaint();
		}
		try {
			if (action.equals("lint")) {
				if (!cp.plot)
					throw new GhostIntegralException();
				integral_Type.setText("Left Integral");
				cp.leftInt = true;
				cp.rightInt = false;
				cp.middleInt = false;
				cp.trapInt = false;
				integral_Type.remove(left_Integral);
				integral_Type.add(clear_Integral);
				integral_Type.add(right_Integral);
				integral_Type.add(middle_Integral);
				integral_Type.add(trapezoidal_Integral);
				cp.repaint();
				c.calculateLeft(d.frames, d.frameLength, d.frameValue);
			}
			if (action.equals("rint")) {
				if (!cp.plot)
					throw new GhostIntegralException();
				integral_Type.setText("Right Integral");
				cp.leftInt = false;
				cp.rightInt = true;
				cp.middleInt = false;
				cp.trapInt = false;
				integral_Type.remove(right_Integral);
				integral_Type.add(clear_Integral);
				integral_Type.add(left_Integral);
				integral_Type.add(middle_Integral);
				integral_Type.add(trapezoidal_Integral);
				cp.repaint();
				c.calculateRight(d.frames, d.frameLength, d.frameValue);
			}
			if (action.equals("mint")) {
				if (!cp.plot)
					throw new GhostIntegralException();
				integral_Type.setText("Middle Integral");
				cp.leftInt = false;
				cp.rightInt = false;
				cp.middleInt = true;
				cp.trapInt = false;
				integral_Type.remove(middle_Integral);
				integral_Type.add(clear_Integral);
				integral_Type.add(left_Integral);
				integral_Type.add(right_Integral);
				integral_Type.add(trapezoidal_Integral);
				cp.repaint();
				c.calculateMiddle(d.frames, d.frameLength, d.frameValue);
			}
			if (action.equals("tint")) {
				if (!cp.plot)
					throw new GhostIntegralException();
				integral_Type.setText("Trapezoidal Integral");
				cp.leftInt = false;
				cp.rightInt = false;
				cp.middleInt = false;
				cp.trapInt = true;
				integral_Type.remove(trapezoidal_Integral);
				integral_Type.add(clear_Integral);
				integral_Type.add(left_Integral);
				integral_Type.add(right_Integral);
				integral_Type.add(middle_Integral);
				cp.repaint();
				c.calculateTrap(d.frames, d.frameLength, d.frameValue);
			}
		}
		catch (GhostIntegralException gie) {
			JOptionPane.showMessageDialog(cp, "No function drawn", "ERROR",
			        JOptionPane.ERROR_MESSAGE);
		}
		if (action.equals("cint")) {
			integral_Type.setText("Integral Type");
			cp.leftInt = false;
			cp.rightInt = false;
			cp.middleInt = false;
			cp.trapInt = false;
			integral_Type.remove(clear_Integral);
			integral_Type.add(left_Integral);
			integral_Type.add(right_Integral);
			integral_Type.add(middle_Integral);
			integral_Type.add(trapezoidal_Integral);
			cp.repaint();
		}
	}
}
