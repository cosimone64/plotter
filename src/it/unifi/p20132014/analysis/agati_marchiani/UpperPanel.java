package it.unifi.p20132014.analysis.agati_marchiani;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

@SuppressWarnings("serial")
public class UpperPanel extends JPanel implements ActionListener {
	private CartesianPlane cp;
	private JLabel function;
	private JLabel differentiable;
	private JLabel left_Extreme;
	private JLabel right_Extreme;
	private JLabel num_Draw_Points;
	private JLabel starting_Point;
	private JLabel num_Iter_Rect;
	private JLabel epsilon;
	protected JTextField text_function;
	protected JCheckBox checkBox;
	protected JTextField text_left_Extreme;
	protected JTextField text_right_Extreme;
	protected JTextField text_num_Draw_Points;
	protected JTextField text_starting_Point;
	protected JTextField text_num_Iter_Rect;
	protected JTextField text_epsilon;

	/**
	 * This is the constructor of the component. Every element is initialized
	 * and added to the panel. An action listener is added to the checkbox.
	 */
	public UpperPanel(CartesianPlane cp) {
		this.cp = cp;
		function = new JLabel("Function");
		text_function = new JTextField();
		differentiable = new JLabel("Differentiable");
		checkBox = new JCheckBox();
		left_Extreme = new JLabel("Left Extreme");
		text_left_Extreme = new JTextField();
		right_Extreme = new JLabel("Right Extreme");
		text_right_Extreme = new JTextField();
		num_Draw_Points = new JLabel("Num. draw points");
		text_num_Draw_Points = new JTextField();
		starting_Point = new JLabel("Starting Point");
		text_starting_Point = new JTextField();
		num_Iter_Rect = new JLabel("Num. iter./rect.");
		text_num_Iter_Rect = new JTextField();
		epsilon = new JLabel("Epsilon");
		text_epsilon = new JTextField();

		setPreferredSize(new Dimension(1024, 50));
		setLayout(new GridLayout(2, 8));
		add(function); // label
		add(differentiable); // label
		add(left_Extreme);
		add(right_Extreme);
		add(num_Draw_Points);
		add(starting_Point);
		add(num_Iter_Rect);
		add(epsilon);
		add(text_function);
		add(checkBox);
		add(text_left_Extreme);
		add(text_right_Extreme);
		add(text_num_Draw_Points);
		add(text_starting_Point);
		add(text_num_Iter_Rect);
		add(text_epsilon);

		checkBox.addActionListener(this);
		checkBox.setActionCommand("check");
	}

	/**
	 * This method inverts the boolean value of cp.diff if the user
	 * checks/unchecks the checkbox.
	 */
	public void actionPerformed(ActionEvent e) {
		String action = e.getActionCommand();
		if (action.equals("check"))
			cp.diff = !cp.diff;
	}
}
